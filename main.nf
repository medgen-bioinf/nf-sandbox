process sayHello {

  container 'registry.gitlab.ics.muni.cz:443/medgen-bioinf/nano-pod'
  executor 'local'

  output:
  stdout result

  """
  echo 'Hello world! ${baseDir}' > file.dir
  ls ${baseDir} > file.ls
  """
}